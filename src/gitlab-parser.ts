export enum GitlabEventType {
  PUSH, TAG, ISSUE, NOTE, MERGE_REQUEST, WIKI, PIPELINE, BUILD
}

export function guessEnumFromObjectKind(objectKind: string): GitlabEventType {
  switch (objectKind) {
    case 'build':
      return GitlabEventType.BUILD
    case 'issue':
      return GitlabEventType.ISSUE
    case 'merge_request':
      return GitlabEventType.MERGE_REQUEST
    case 'note':
      return GitlabEventType.NOTE
    case 'pipeline':
      return GitlabEventType.PIPELINE
    case 'push':
      return GitlabEventType.PUSH
    case 'tag_push':
      return GitlabEventType.TAG
    case 'wiki_page':
      return GitlabEventType.WIKI
    default:
      throw new Error(`could not find a Gitlab Event type for ${objectKind}`)
  }
}

export class GitlabNews {
  constructor(readonly project: string,
              readonly eventType: GitlabEventType,
              readonly userName: string,
              readonly userAvatar: string,
              readonly gitlabNews: any) {
  }
}

export class GitlabParser {
  static parse(news: any): GitlabNews {
    console.log('news -> ', JSON.stringify(news, null, 2))
    let eventType = guessEnumFromObjectKind(news['object_kind'])
    let project
    let userName
    let userAvatar
    // default values:
    project = `${news.project.namespace}/${news.project.name}`
    userName = news['user_name']
    userAvatar = news['user_avatar']
    switch (eventType) {
      case GitlabEventType.ISSUE:
        userAvatar = news.user['avatar_url']
        userName = news.user.username
        break
      default:
    }
    return new GitlabNews(
        project,
        eventType,
        userName,
        userAvatar,
        news
    )
  }
}
